#!/bin/bash

# To run this script, do 
# qsub -t 1-n submit_ega_encrypt.sh <files>
#
#$ -N ega_encrypt
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=2:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

EGA_CRYPTOR=/exports/igmm/eddie/ISARIC4C/wp5-rna-seq/analysis/software/ega-cryptor-2.0.0.jar

FILES=$1
BASE=`head -n $SGE_TASK_ID $FILES | tail -n 1`

mkdir -p ../encrypted

READS=`ls $BASE* | tr '\n' ','`

java -Xmx8g -jar $EGA_CRYPTOR -i "$READS" -t 1 -o ../encrypted
