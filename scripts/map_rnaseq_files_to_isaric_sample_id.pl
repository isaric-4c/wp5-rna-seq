#!/usr/bin/perl -w

use IO::File;
use strict;

my $samples_file = shift;
my $read1_file = shift;
my $read2_file = shift;

my $samples_fh = new IO::File;
$samples_fh->open($samples_file, "r") or die "Could not open $samples_file\n$!";

my %sample_map;
while (my $line = <$samples_fh>)
{
    next if ($line =~ /rnaseq/);

    chomp $line;
    $line =~ s/\"//g;
    my ($rnaseq_sample_id, $isaric_sample_id, $subjid, $lims_timepoint, $date_taken)  = split(',', $line);

    if ($rnaseq_sample_id =~ /\_NS2000\_75/)
    {
	$rnaseq_sample_id = substr($rnaseq_sample_id, 0, -10);
    }

    my @read1_files = split(/\n/, `grep $rnaseq_sample_id $read1_file`);
    my @read2_files = split(/\n/, `grep $rnaseq_sample_id $read2_file`);

    for (my $i = 0; $i < scalar(@read1_files); $i++)
    {
	my @tokens1 = split(/\//, $read1_files[$i]);
	my $read1 = join('_', @tokens1[7..8]);

	my @tokens2 = split(/\//, $read2_files[$i]);
	my $read2 = join('_', @tokens2[7..8]);

	print "$isaric_sample_id,$read1,$read2\n";
    }
}
