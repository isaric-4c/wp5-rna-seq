#!/usr/bin/perl -w

=head1 NAME

create_sample_sheet.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Creates the samplesheet.csv file formatted for input to the nf-core/viralrecon
pipeline. Input is the absolute paths to the R1 read files; assumes R2 file paths
are identical except for 1/2 and that sample names parse out as the basename of
the path minus '_R1/2_val_1/2.fq.gz'.

=cut

use strict;

# Perl
use File::Basename;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help] < `ls /path/to/trim_galore/*1.fq.gz' > batch_viralrecon_samplesheet.csv
};

my $help = 0;

GetOptions(
    'help'     => \$help,
    ) or die $usage;

if ($help)
{
    print $usage;
    exit(0);
}

print "sample,fastq_1,fastq_2\n";
while (my $line = <>)
{
    chomp $line;

    my $sample = basename($line);
    $sample =~ s/_R1_val_1.fq.gz//;
    my $fastq1 = $line;
    my $fastq2 = $line;
    $fastq2 =~ s/_R1_val_1/_R2_val_2/;

    print "$sample,$fastq1,$fastq2\n";
}
