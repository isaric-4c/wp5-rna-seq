# WP5 RNA-seq

Analysis of poly-A selected RNA sequencing from human whole blood samples of COVID-19 patients.

See the [wiki](https://git.ecdf.ed.ac.uk/isaric-4c/wp5-rna-seq/wikis/home) for documentation.
